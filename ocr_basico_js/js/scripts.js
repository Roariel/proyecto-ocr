
function total(){

  var total = 0;

  $(".monto").each(function(){
      var valor = parseFloat($(this).val());
    if (typeof valor == "number"){
          total += valor;
    }
  });

  $("#total").val(total);
};

function calcularPorcentaje(){

  var montoAnual = parseFloat($("#montoAnual").val());
  var total = parseFloat($("#total").val());
  var porcentaje = parseFloat((total*100)/montoAnual);

  $(".progress-bar").css("width", porcentaje +"%");
  $(".progress-bar").text(porcentaje +"%");


  if (porcentaje <= 20){
    $(".progress-bar").removeClass("bg-warning");
    $(".progress-bar").removeClass("bg-success");
    $(".progress-bar").addClass("bg-danger");
  }else{
    if (porcentaje >=21 && porcentaje <=74){
      $(".progress-bar").removeClass("bg-danger");
      $(".progress-bar").removeClass("bg-success");
      $(".progress-bar").addClass("bg-warning");
    }else{
      if (porcentaje >= 75){ 
          $(".progress-bar").removeClass("bg-warning");
          $(".progress-bar").removeClass("bg-danger");      
          $(".progress-bar").addClass("bg-success");}
    }
  }
};

function faltantenMonto(){

  $("#falta").val(parseFloat($("#montoAnual").val()));
};

function faltaCubrir(){

  var montoAnual = parseFloat($("#montoAnual").val());
  var total = parseFloat($("#total").val());

  var faltan = parseFloat(montoAnual - total);

  $("#falta").val(faltan);
};

function sumar_nov(){

  $("#bt_sum_nov").click(function(){

      var add_nov = parseFloat($("#sum_nov").val());
      var acc_nov = parseFloat($("#nov").val());
      var new_monto = parseFloat(acc_nov+add_nov);

      $("#nov").val(new_monto);
  });
};

function update() {
  $(".monto").change(function() {
    total();
    faltaCubrir();
    calcularPorcentaje();
  });
};




