import unittest
from clases.comunidad import Comunidad
from clases.persona import Persona

class TestOCR(unittest.TestCase):

	def setUp(self):
		self.profu2 = Comunidad("PROFU2",800)
		self.rodrigo = Persona("Rodrigo","Gonzalez",self.profu2)

		self.profu2.set_integrantes([self.rodrigo])
		self.rodrigo.aportar(200)
	
	def test_aportar(self):
		
		self.assertEqual(self.rodrigo.get_aportado(), 200,"ERROR")

if __name__ == "__main__":
	unittest.main()