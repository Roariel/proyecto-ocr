class Comunidad:

    integrantes = []

    def __init__(self,nombre, ofrenda):
        self.nombre = nombre
        self.ofrenda = ofrenda
        self.ofrenda_comunitaria = 0

    def set_nombre(self,name):
        self.nombre = name

    def set_integrantes(self,integrantes):
        for p in integrantes:
            self.integrantes.append(p)

    def set_nuevo_integrante(self, persona):
        self.integrantes.append(persona)

    def get_nombre(self):
        return self.nombre

    def get_integrantes(self):
        return self.integrantes

    def get_ofrenda(self):
        return self.ofrenda

    def cantidad_de_integrantes(self):
        return len(self.get_integrantes())


    def calcular_ofrenda_comunitaria(self):
        self.ofrenda_comunitaria = self.cantidad_de_integrantes() * self.get_ofrenda()

    def get_ofrenda_comunitaria(self):
        return self.ofrenda_comunitaria

    def get_total_acomulado(self):
        total_acomulado = 0
        for i in self.get_integrantes():
            total_acomulado = total_acomulado + i.get_aportado()

        return total_acomulado

    def get_faltante_ofrenda(self):
        return self.get_ofrenda_comunitaria() - self.get_total_acomulado()
