class Persona:

    def __init__(self, nombre,ape, comu):
        self.nombre = nombre
        self.apellido = ape
        self.comunidad = comu
        self.ofrenda = 0
        self.aportado = 0
        self.restante_de_ofrenda = 0

    def set_nombre(self,name):
        self.nombre = name

    def set_apellio(self,ape):
        self.apellido = ape

    def set_comunidad(self,comu):
        self.comunidad = comu

    def get_nombre(self):
        return self.nombre

    def get_apellido(self):
        return self.apellido

    def get_comunidad(self):
        return self.comunidad

    def aportar(self,monto):
        self.aportado = self.aportado + monto

    def set_ofrenda(self):
        self.ofrenda = self.comunidad.get_ofrenda()       

    def get_aportado(self):
        return self.aportado

    def get_restante_de_ofrenda(self):
        return ((self.get_ofrenda()) - (self.get_aportado()))
