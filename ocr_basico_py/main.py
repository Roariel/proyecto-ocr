from clases.comunidad import Comunidad
from clases.persona import Persona

profu2 = Comunidad("PROFU2",800)

rodrigo = Persona("Rodrigo","Gonzalez",profu2)
miriam = Persona("Miriam","Maciel",profu2)
joha = Persona("Johana","Pachado",profu2)

profu2.set_integrantes([rodrigo,miriam,joha])
profu2.calcular_ofrenda_comunitaria()

rodrigo.aportar(200)
miriam.aportar(300)
joha.aportar(250)

print("Este es el objetivo comunitario:" , profu2.get_ofrenda_comunitaria())
print(rodrigo.get_nombre(), "aporto: ",  rodrigo.get_aportado())
print(miriam.get_nombre(),  "aporto: ",  miriam.get_aportado())
print(joha.get_nombre(), "aporto: ", joha.get_aportado())
print("El total ofrendado es: ", profu2.get_total_acomulado())
print("El resto a ofrendar es: ", profu2.get_faltante_ofrenda())